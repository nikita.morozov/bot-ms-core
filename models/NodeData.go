package models

import "strconv"

type NodeData map[string][]byte

const NodeDataOnStart = "OnStart"
const NodeDataOnComplete = "OnComplete"
const NodeDataOnError = "OnError"

func NodeDataFromParams(params map[string][]byte) NodeData {
	res := NodeData{}

	for key, value := range params {
		res[key] = value
	}

	return res
}

func NodeDataError(text string) NodeData {
	return NodeData{}.Error(text)
}

func NodeDataComplete(text *string) NodeData {
	return NodeData{}.Complete(text)
}

func (nd NodeData) Complete(text *string) NodeData {
	res := "true"
	if text != nil {
		res = *text
	}

	nd[NodeDataOnComplete] = []byte(res)

	return nd
}

func (nd NodeData) Error(text string) NodeData {
	nd[NodeDataOnError] = []byte(text)
	return nd
}

func (nd NodeData) SetEvent(key string) NodeData {
	nd[key] = []byte("true")
	return nd
}

func (nd *NodeData) Set(key string, value string) {
	(*nd)[key] = []byte(value)
}

func (nd *NodeData) SetUint(key string, value uint) {
	(*nd)[key] = []byte(strconv.FormatUint(uint64(value), 10))
}

func (nd *NodeData) SetInt(key string, value int) {
	(*nd)[key] = []byte(strconv.FormatInt(int64(value), 10))
}

func (nd *NodeData) SetBool(key string, value bool) {
	(*nd)[key] = []byte(strconv.FormatBool(value))
}

func (nd NodeData) IsExist(key string) bool {
	_, ok := nd[key]

	return ok
}

func (nd NodeData) IsComplete() bool {
	_, ok := nd[NodeDataOnComplete]

	return ok
}

func (nd NodeData) IsError() bool {
	_, ok := nd[NodeDataOnError]

	return ok
}

func (nd NodeData) GetError() string {
	val, ok := nd[NodeDataOnError]
	if !ok {
		return ""
	}

	return string(val)
}

func (np NodeData) GetString(key string) string {
	return string(np[key])
}

func (np NodeData) GetBytes(key string) []byte {
	return np[key]
}

func (np *NodeData) SetByte(key string, value []byte) {
	(*np)[key] = value
}

func (np NodeData) GetUint(key string) uint {
	val, err := strconv.ParseUint(string(np[key]), 10, 64)
	if err != nil {
		return 0
	}

	return uint(val)
}

func (np NodeData) GetBool(key string) bool {
	val, err := strconv.ParseBool(string(np[key]))
	if err != nil {
		return false
	}

	return val
}
