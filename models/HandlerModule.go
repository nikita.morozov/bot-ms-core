package models

type ModuleHandler struct {
	Name    string
	Handler IHandler
}

type ModuleSeed struct {
	Name    string
	Actions []Action
}

type HandlerModule interface {
	GetName() string
	GetHandlers() []ModuleHandler
	GetSeeds() []ModuleSeed
}
