package models

import "strconv"

type NodeParams map[string][]byte

func NodeParamsFromData(data map[string][]byte) NodeParams {
	res := NodeParams{}

	for key, value := range data {
		res[key] = value
	}

	return res
}

func NodeParamsFromSource(data NodeParams) NodeParams {
	if data != nil {
		return data
	}

	return NodeParams{}
}

func (np NodeParams) GetUint(key string) uint {
	val, err := strconv.ParseUint(string(np[key]), 10, 64)
	if err != nil {
		return 0
	}

	return uint(val)
}

func (np NodeParams) GetInt(key string) uint {
	val, err := strconv.ParseInt(string(np[key]), 10, 64)
	if err != nil {
		return 0
	}

	return uint(val)
}

func (np NodeParams) GetBool(key string) bool {
	val, err := strconv.ParseBool(string(np[key]))
	if err != nil {
		return false
	}

	return val
}

func (np *NodeParams) Set(key string, value string) {
	(*np)[key] = []byte(value)
}

func (np NodeParams) GetString(key string) string {
	return string(np[key])
}

func (np NodeParams) GetByte(key string) []byte {
	return np[key]
}

func (np *NodeParams) SetByte(key string, value []byte) {
	(*np)[key] = value
}

func (np *NodeParams) SetUint(key string, value uint) {
	(*np)[key] = []byte(strconv.FormatUint(uint64(value), 10))
}

func (np *NodeParams) SetInt(key string, value int) {
	(*np)[key] = []byte(strconv.FormatInt(int64(value), 10))
}

func (np *NodeParams) SetBool(key string, value bool) {
	(*np)[key] = []byte(strconv.FormatBool(value))
}
