package models

type IHandler interface {
	Execute(params NodeParams) NodeData
}
