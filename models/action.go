package models

type Action struct {
	Id      uint     `json:"id" gorm:"primarykey"`
	Name    string   `json:"name"`
	Handler string   `json:"handler"`
	Scheme  string   `json:"scheme"`
	Sockets []Socket `json:"sockets"`
}
