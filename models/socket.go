package models

const SocketDirectionIn = uint8(0)
const SocketDirectionOut = uint8(1)

const SocketTypeString = uint8(0)
const SocketTypeObject = uint8(1)
const SocketTypeInt = uint8(2)
const SocketTypeEmpty = uint8(3)
const SocketTypeEvent = uint8(4)
const SocketTypeBool = uint8(5)

type Socket struct {
	Id        uint   `json:"id" gorm:"primarykey"`
	Direction uint8  `json:"direction"`
	Type      uint8  `json:"type"`
	Title     string `json:"title"`
	Slug      string `json:"slug"`
	ActionId  uint   `json:"actionId"`
	Manual    bool   `json:"manual"`
}
